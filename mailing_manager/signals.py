from django.dispatch import receiver
from django.dispatch import Signal
from . import models
from django.utils import timezone
from .tasks import send_messages
from django.db.models.signals import post_save
from django.dispatch import receiver

user_signal = Signal()

@receiver(post_save, sender=models.Mailing)
def your_handler(sender, instance, **kwargs):
    """
    Signal for sending messages
    """
    print('**********сработал сигнал для рассылки**************')

    clients = models.Client.objects.all()

    mailing_dict = models.Mailing.objects.values(
        "id", "filter_mobile_operator_code", "filter_tag", "end_time", "start_time").get(pk=instance.pk)

    # mailing_id = mailing_dict.get("id")
    # mailing_filter_mobile_operator_code = mailing_dict.get("filter_mobile_operator_code")
    # mailing_filter_tag = mailing_dict.get("filter_tag")
    # mailing_end_time = mailing_dict.get("end_time")
    # mailing_start_time = mailing_dict.get("start_time")
    if mailing_dict.get("filter_mobile_operator_code"):
        clients = clients.filter(operator_code=mailing_dict.get("filter_mobile_operator_code"))
    if mailing_dict.get("filter_tag"):
        clients = clients.filter(tag=mailing_dict.get("filter_tag"))


    if timezone.now() <= mailing_dict.get("end_time"):

        list_of_clients = []
        for client in clients:
            list_of_clients.append(models.Message(status='Not sent', mailing=instance, client=client))
        models.Message.objects.bulk_create(list_of_clients)

        if mailing_dict.get("start_time") <= timezone.now():
            send_messages.apply_async(
                (mailing_dict.get("id"), mailing_dict.get("text")),
                expires=mailing_dict.get("end_time"),
            )
        else:
            send_messages.apply_async(
                (mailing_dict.get("id"), mailing_dict.get("text")),
                eta=mailing_dict.get("start_time"),
                expires=mailing_dict.get("end_time"),
            )
