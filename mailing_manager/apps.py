from django.apps import AppConfig
from django.core.signals import request_finished


class MailingManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mailing_manager'

    def ready(self):
        from . import signals
